// Jordan Fisher - Playing Cards

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank
{
	TWO = 2, THREE, FOUR, FIVE,
	SIX, SEVEN, EIGHT, NINE, TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

enum Suit
{
	CLUBS,
	HEARTS,
	SPADES,
	DIAMONDS
};

struct Card 
{
	Rank rank;
	Suit suit;
};

int main() 
{
	

	_getch();
	return 0;
}